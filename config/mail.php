<?php

return [

    'driver' => env('MAIL_DRIVER', 'smtp'),
    'host'       => env('MAIL_HOST'),
    'port'   => env('MAIL_PORT', 587),
    'encryption' => env('mail_encryption'),
    'username'   => env('MAIL_USERNAME'),
    'password'   => env('MAIL_PASSWORD'),
    'transport'  => 'smtp',

    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'hello@example.com'),
        'name'    => env('MAIL_FROM_NAME', 'Example'),
    ],

    'ses' => [
        'transport' => 'ses',
    ],
];
