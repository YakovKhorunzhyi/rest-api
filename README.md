# Installation #

Copy .env.example to .env:
```
cp .env.example .env
```

Execute commands for start project:

```
$ sudo chmod +x helper
$ sudo ./helper start
```

# Api commands: #

Open folder http-requests

Paste all commands to postman and execute them
