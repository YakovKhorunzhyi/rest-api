FROM php:7.4-fpm

ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y \
       wget \
       curl \
       nano \
       htop \
       git \
       unzip \
       bzip2 \
       software-properties-common \
       locales \
       libxml2-dev

# Install PostgreSQL PDO driver and PostgreSQL extension
RUN apt-get install -y libpq-dev
RUN docker-php-ext-install pdo_pgsql pgsql

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . /var/www
WORKDIR /var/www
EXPOSE 80
