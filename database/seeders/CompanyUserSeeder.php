<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\User;
use App\Models\CompanyUser;
use Illuminate\Database\Seeder;

class CompanyUserSeeder extends Seeder
{
    private int $companiesWithMoreThanOneUser = 5;
    private int $minUsersInCompany = 2;
    private int $maxUsersInCompany = 10;

    public function run()
    {
        CompanyUser::factory()->count(15)->create();

        $companies = Company::all('id')
                            ->shift($this->companiesWithMoreThanOneUser);

        foreach ($companies as $company) {
            for ($i = 0; $i < $this->getUsersQuantity(); $i++) {
                $userCompany = new CompanyUser();

                $userCompany->user_id    = User::factory()->create()->id;
                $userCompany->company_id = $company->id;

                $userCompany->save();
            }
        }
    }

    private function getUsersQuantity(): int
    {
        return rand($this->minUsersInCompany, $this->maxUsersInCompany);
    }
}
