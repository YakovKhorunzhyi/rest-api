<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\CompanyUser;
use App\Models\Company;
use App\Models\User;

class CompanyUserFactory extends Factory
{
    protected $model = CompanyUser::class;

    public function definition()
    {
        return [
            'user_id'    => User::factory()->create()->id,
            'company_id' => Company::factory()->create()->id,
        ];
    }
}
