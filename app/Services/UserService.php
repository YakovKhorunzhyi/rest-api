<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UserService
{
    public function create(array $user): User
    {
        $user['password'] = Hash::make($user['password']);
        $user['hash']     = Str::random(64);

        return User::create($user);
    }

    public function getRepository(): UserRepository
    {
        return new UserRepository();
    }
}
