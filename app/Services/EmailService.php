<?php

namespace App\Services;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Message;
use App\Models\User;

class EmailService
{
    public function confirm(int $userId, string $hash): void
    {
        $user = User::where('id', '=', $userId)->first();

        if ($user === null || $user->hash !== $hash) {
            throw new NotFoundHttpException('Confirmation info not found');
        }

        $user->hash      = null;
        $user->confirmed = 1;
        $user->save();
    }

    public function sendRecoverPasswordMessage(User $user): void
    {
        $message =  '{frontend_url}' . "?user_id={$user->id}&hash={$user->hash}";

        Mail::raw(
            $message,
            function (Message $message) use ($user) {
                $message->to($user->email);
                $message->from(env('MAIL_FROM_ADDRESS'));
            }
        );
    }

    public function sendConfirmRegistration(User $user): void
    {
        $message = route('email-confirm') . "?user_id={$user->id}&hash={$user->hash}";

        Mail::raw(
            $message,
            function (Message $message) use ($user) {
                $message->to($user->email);
                $message->from(env('MAIL_FROM_ADDRESS'));
            }
        );
    }
}
