<?php

namespace App\Services;

use App\Repositories\CompanyRepository;
use App\Models\Company;

class CompanyService
{
    public function create(array $company, int $userId): Company
    {
        $company = Company::create($company);
        $company->users()->attach($userId);

        return $company;
    }

    public function getRepository(): CompanyRepository
    {
        return new CompanyRepository();
    }
}
