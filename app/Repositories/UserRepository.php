<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function firstByOrFail(string $paramName, $value): User
    {
        return User::where($paramName, $value)->firstOrFail();
    }
}
