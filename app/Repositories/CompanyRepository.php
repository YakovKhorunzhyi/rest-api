<?php

namespace App\Repositories;

use App\Models\Company;

class CompanyRepository
{
    public function firstByOrFail(string $paramName, $value): Company
    {
        return Company::where($paramName, $value)->firstOrFail();
    }
}
