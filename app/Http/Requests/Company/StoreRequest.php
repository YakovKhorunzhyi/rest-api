<?php

namespace App\Http\Requests\Company;

use Anik\Form\FormRequest;

class StoreRequest extends FormRequest
{
    protected function rules(): array
    {
        return [
            'title'       => 'required|min:3|max:255',
            'phone'       => 'required|min:12|max:12',
            'description' => 'required|min:3|max:255',
        ];
    }
}
