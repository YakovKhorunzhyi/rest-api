<?php

namespace App\Http\Requests\Auth;

use Anik\Form\FormRequest;

class SignInRequest extends FormRequest
{
    protected function rules(): array
    {
        return [
            'email'    => 'required|email',
            'password' => 'required|min:3|max:60',
        ];
    }
}
