<?php

namespace App\Http\Requests\Auth;

use Anik\Form\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    protected function rules(): array
    {
        return [
            'password' => 'required|string',
            'hash'     => 'required|string',
            'user_id'  => 'required|integer',
        ];
    }
}
