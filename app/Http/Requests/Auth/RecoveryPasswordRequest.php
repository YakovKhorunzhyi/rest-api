<?php

namespace App\Http\Requests\Auth;

use Anik\Form\FormRequest;

class RecoveryPasswordRequest extends FormRequest
{
    protected function rules(): array
    {
        return [
            'email' => 'required|email',
        ];
    }
}
