<?php

namespace App\Http\Requests\User;

use Anik\Form\FormRequest;

class StoreRequest extends FormRequest
{
    protected function rules(): array
    {
        return [
            'first_name' => 'required|min:3|max:255',
            'last_name'  => 'required|min:3|max:255',
            'email'      => 'required|email|unique:users',
            'phone'      => 'required|min:12|max:12',
            'password'   => 'required|min:3|max:60',
        ];
    }
}
