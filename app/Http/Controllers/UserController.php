<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Http\Requests\Auth\RecoveryPasswordRequest;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\SignInRequest;
use App\Http\Requests\User\StoreRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Services\EmailService;
use App\Services\UserService;
use App\Models\EmailConfirm;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserController extends Controller
{
    protected UserService  $service;
    protected EmailService $emailService;

    public function __construct(EmailService $emailService, UserService $service)
    {
        $this->emailService = $emailService;
        $this->service      = $service;
    }

    ////////////////////////////////////////////////////////

    public function register(StoreRequest $request)
    {
        $user = $this->service->create($request->validated());
        $this->emailService->sendConfirmRegistration($user);

        return $this->createdResponse();
    }

    public function signIn(SignInRequest $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!($token = Auth::attempt($credentials))) {
            throw new NotFoundHttpException();
        }

        if (!$request->user()->confirmed) {
            throw new NotFoundHttpException('User is not confirmed');
        }

        return $this->createdResponse([
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60 * 24,
            'access_token' => $token,
        ]);
    }

    public function emailConfirm(Request $request)
    {
        $this->emailService->confirm((int)$request->get('user_id'), $request->get('hash'));

        return $this->successResponse();
    }

    public function recoverPassword(RecoveryPasswordRequest $request)
    {
        $user = $this->service->getRepository()->firstByOrFail('email', $request->get('email'));
        $user->hash = Str::random(64);
        $user->save();
        $this->emailService->sendRecoverPasswordMessage($user);

        return $this->createdResponse();
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = $this->service->getRepository()->firstByOrFail('id', $request->get('user_id'));
        if ($request->get('hash') === $user->hash) {
            $user->password = Hash::make($request->get('password'));
            $user->hash = null;
            $user->save();
        }

        return $this->noContentResponse();
    }

    public function show(Request $request)
    {
        return $this->successResponse($request->user()->companies->toArray());
    }
}
