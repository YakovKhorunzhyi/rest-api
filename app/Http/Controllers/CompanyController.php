<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\Company\StoreRequest;
use App\Services\CompanyService;

class CompanyController extends Controller
{
    protected CompanyService $service;

    public function __construct(CompanyService $service)
    {
        $this->service = $service;
    }

    public function create(StoreRequest $request)
    {
        $this->service->create($request->validated(), $request->user()->id);

        return $this->createdResponse();
    }
}
