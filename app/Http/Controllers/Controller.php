<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function createdResponse(array $data = [])
    {
        return response()->json(['data' => $data], 201);
    }

    public function successResponse(array $data = [])
    {
        return response()->json(['data' => $data], 200);
    }

    public function noContentResponse()
    {
        return response()->json(['data' => []], 204);
    }
}
