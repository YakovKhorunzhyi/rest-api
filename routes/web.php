<?php

declare(strict_types=1);

/**
 * @var \Laravel\Lumen\Routing\Router $router
 */

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('user/sign-in', ['uses' => 'UserController@signIn']);

    $router->post('user/register', ['uses' => 'UserController@register']);

    $router->get('user/email-confirm', ['as' => 'email-confirm', 'uses' => 'UserController@emailConfirm']);

    $router->post('user/recover-password', ['uses' => 'UserController@recoverPassword']);

    $router->post('user/change-password', ['uses' => 'UserController@changePassword']);
});

$router->group(['prefix' => 'api', 'middleware' => 'auth:api'], function () use ($router) {
    $router->get('user/companies', ['uses' => 'UserController@show']);

    $router->post('user/companies', ['uses' => 'CompanyController@create']);
});
